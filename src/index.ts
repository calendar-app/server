import express from 'express';
import * as bodyParser from 'body-parser';
import morgan from 'morgan';
import cors from 'cors';
import mongoose from 'mongoose';
import { AddressInfo } from 'net';
import { CalendarRouter } from './api/calendar/calendar.router';
import { resolve } from "path"
import { config } from "dotenv"
config({ path: resolve(__dirname, '../.env') })

export class App {
  protected calendarRouter: CalendarRouter;
  constructor() {
    this.calendarRouter = new CalendarRouter()
  }

  app: express.Application;
  mongo_uri = process.env.MONGO_ADDRESS || 'mongodb://mongodb:27017/calendar';
  port = process.env.PORT || 7000;

  async init() {
    this.app = express();
    this.app.use(cors());
    this.app.use(morgan('common'));
    this.app.use(bodyParser.json());

    try {
      await mongoose.connect(this.mongo_uri, { useNewUrlParser: true })
      if (process.env.NODE_ENV === 'development') {
        await mongoose.set('debug', true);
      }
      console.log('Connected to MongoDB');
    } catch (err) {
      console.log(err.message);
      throw err;
    }

    this.app.use(this.calendarRouter.router);
    const server = this.app.listen(this.port, () => {
      const { address, port } = server.address() as AddressInfo;
      console.log(`IP:${address}:${port}`);
    });
  }
}

function start() {
  const app = new App();
  app.init();
}

try {
  if (process.env.NODE_ENV === 'production') {
    setTimeout(() => start(), 10000)
  } else {
    start();
  }
} catch (err) {
  throw err;
}
