import { Document, Schema, Model, model } from "mongoose";

export var CalendarSchema: Schema = new Schema({
  courseName: String,
  timeStart: String,
  timeEnd: String,
  date: Date,
  required: Boolean,
  module: String,
  professor: String,
  room: String
});

export interface ICalendar extends Document {
  courseName?: String,
  timeStart?: String,
  timeEnd?: String,
  date?: Date,
  required?: Boolean,
  module?: String,
  professor?: String,
  room?: String
}

export const Calendar: Model<ICalendar> = model<ICalendar>("Calendar", CalendarSchema);
